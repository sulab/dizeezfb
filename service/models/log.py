from . import db
import datetime

class Log(db.Model):
  id = db.Column(db.Integer,primary_key = True)
  choice_id = db.Column(db.Integer)
  choice_text = db.Column(db.String(100))
  parent_question = db.Column(db.Integer)
  correct = db.Column(db.Integer)
  timestamp = db.Column(db.DateTime)
  game_id = db.Column(db.Integer,db.ForeignKey('game.id'))

  def __init__(self,choice_id = -1, choice_text = "foo", parent_question = -1, correct = 0, game_id = -1):
    self.choice_id = choice_id
    self.choice_text = choice_text
    self.parent_question = parent_question
    self.correct = correct
    self.game_id = game_id
    self.timestamp = datetime.now()

  def __repr__(self):
    return "<log_id %d, game_id %d , choice_id %d>"%(self.id,self.game_id,self.choice_id)
