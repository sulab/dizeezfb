from . import db
import datetime

class Choice(db.Model):
  id            = db.Column(db.Integer, primary_key = True)
  text          = db.Column(db.String(240))
  created       = db.Column(db.DateTime)

  def __init__(self, disease_name ):
    self.text = disease_name
    self.created = datetime.now()

  def __repr__(self):
    return "<Choice : %s>"%(self.text)




