from flask import Flask, request, jsonify, url_for, redirect, render_template, session
from flask.views import View
from datetime import datetime
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.sql.expression import func
from flask.ext.restful import reqparse, abort, Api, Resource
import base64,hashlib
import json, facebook, config, random, sys, urllib
from flask.ext.admin import Admin, BaseView, expose
from flask.ext import admin
from flask.ext.admin.contrib.sqlamodel import ModelView
from flask.ext.admin.actions import action
import urllib, urllib2
#
# A P P  C O N F I G
#

#-- Fetch necessary app parameters to global scope

APP_SECRET = config.APP_SECRET
AUTH_URL = config.AUTH_URL
APP_ID = config.APP_ID
ADMINPASS = config.ADMINPASS
ACCESS_TOKEN = config.ACCESS_TOKEN
ACHIEVEMENT_DOMAIN = config.ACHIEVEMENT_DOMAIN

#-- Global app object
app = Flask(__name__,
            static_url_path = '',
            static_folder = '../web-app',
            template_folder='../web-app' )

#-- Admin Handle
class AdminHomeView(admin.AdminIndexView):

  @expose('/',methods=['GET','POST'])
  def index(self):
    questions = len(Question.query.all())
    categories = len(Category.query.all())
    choices = len(Choice.query.all())
    games = len(Game.query.all())

    if request.method =='GET':
      if not session.get('admin'):
        return render_template('adminlogin.html')
      else:
        return self.render('adminhome.html', questions = questions,categories = categories,choices = choices,games=games)
    else:
      passphrase = request.form.get('passphrase')
      print passphrase
      if passphrase == ADMINPASS:
        session['admin'] = True
        #-- redirect to /admin/
        print "Admin Redirect"
        return redirect('/admin/')
      else:
        return render_template('adminlogin.html')



admin = Admin(app ,index_view = AdminHomeView())

#-- Global restful api handle
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] =  config.CONNECTION_URI
app.secret_key = 'a big secret key'
db = SQLAlchemy(app)

#
# U T I L S
#

def get_questions(api_key='',method=1):
  ''' Accepts api_key and a paramteter method. Method
      determines the criteria by which the questions
      are generated.Currently only one method is de-
      fined. "1 Random Question Selection"
  '''
  #-- Selects 1000 questions at random
  if method ==1:
    questions = Question.query.order_by(func.random()).limit(100).all()
    #questions = questions[1:100]
    return (questions,1)
  #-- Action corresponding to method 2
  elif method == 2:
  #  pass
    questions = []    
    for c in Category.query.all():
      question_list = c.questions
      random.shuffle(question_list)
      if len(question_list) > 20 :
        question_list = question_list[0:20]
      for q in question_list:
        if q.question not in questions:
          questions.append(q.question)
    return (questions,2)
  #-- Action corresponding to method 3 etc,

  elif method == 3:
    pass

  else:
    pass

def create_if_not_choice(choice_text=""):
  '''
      if this choice do exist ,return its id, else
      create that choice and return its id
  '''
  choice = Choice.query.filter_by(text = choice_text).all()
  if not choice:
    c = Choice(choice_text)
    db.session.add(c)
    db.session.commit()
    return c.id
  else:
    return choice[0].id

def create_if_not_category(category_text = ""):
  '''
      if this categor do exist, return its id ,else
      create it and return its id
  '''
  category = Category.query.filter_by(text = category_text).all()
  if not category:
    c = Category(category_text)
    db.session.add(c)
    db.session.commit()
    return c.id
  else:
    return category[0].id

'''
  Admin Section
'''

class AddQuestionView(BaseView):
  def is_accessible(self):
    return session.get('admin')

  @expose('/')
  def index(self):
    return self.render('addquest.html')

class LogoutView(BaseView):
  @expose('/')
  def logout(self):
    session.pop('admin')
    return redirect('/admin')

class QuestionAdmin(ModelView):

  column_list = ['text']
  column_searchable_list = ['text']

  def is_accessible(self):
    return session.get('admin')

  @action('remove', 'Remove', 'Are you sure you want to purge  selected models premenantly?')
  def action_merge(self, ids):
    for i in ids:
      qcassocs = QCAssociation.query.filter_by(question_id = i)
      for qcassoc in qcassocs:
        db.session.delete(qcassoc)
      db.session.commit()
      qcatassocs = QCATAssociation.query.filter_by(question_id = i)
      for qcatassoc in qcatassocs:
        db.session.delete(qcatassoc)
      db.session.commit()
      question = Question.query.get(i)
      db.session.delete(question)
      db.session.commit()

class CategoryAdmin(ModelView):

 column_list = ['text']
 column_searchable_list = ['text']

 def is_accessible(self):
   return session.get('admin')

 @action('remove', 'Remove', 'Are you sure you want to purge  selected models premenantly?')
 def action_merge(self, ids):
   for i in ids:
     qcatassocs = QCATAssociation.query.filter_by(category_id = i)
     for qcatassoc in qcatassocs:
       db.session.delete(qcatassoc)
     db.session.commit()
     category = Category.query.get(i)
     db.session.delete(category)
     db.session.commit()



admin.add_view(QuestionAdmin(Question, db.session))
admin.add_view(ModelView(Choice, db.session))
admin.add_view(CategoryAdmin(Category, db.session))
admin.add_view(AddQuestionView(name='Add Question'))
admin.add_view(LogoutView(name="Logout"))

#
# Main App Center
#

#-- Routes
@app.route('/play',methods=['GET','POST'])
def index():
  api_key=request.cookies.get('api_key')
  user = User.query.filter_by(api_key = api_key)[0]
  category_dict = {}
  
  for c in Category.query.all():
    category_dict[c.id] = c.text

  categorylist = json.dumps(category_dict)
  
  if user.name == "Anonymous":
    level = int(request.cookies.get('level'))
    return render_template('play.html',categories=Category.query.all(),levels=range(level,0,-1),fb_id=user.fb_id,user_name=user.name,latest_level = level,app_id = APP_ID,category_dict = categorylist)
  else:
    return render_template('play.html',categories=Category.query.all(),levels=range(user.level,0,-1),fb_id=user.fb_id,user_name=user.name,latest_level = user.level,app_id = APP_ID,category_dict = categorylist)



@app.route('/choices',methods=['GET'])
def searchchoice():
  choice_list = []
  choices = Choice.query.filter(Choice.text.like(request.args['choicestr']+'%'))
  for choice in choices:
    choice_list.append(choice.text)
  return json.dumps(choice_list)

@app.route('/categories',methods=['GET','POST'])
def searchcategory():
  category_list = []
  categories = Category.query.filter(Category.text.like(request.args['catstr']+ '%'))
  for category in categories:
    category_list.append(category.text)

  return json.dumps(category_list)

@app.route('/questions',methods=['GET','POST'])
def searchquestion():
  question_list = []
  questions = Question.query.filter(Question.text.like(request.args['qststr'] + '%'))
  for question in questions:
    question_list.append(question.text)
  return json.dumps(question_list)

@app.route('/api/v1/updatelevel',methods=['GET','POST'])
def updatelevel():
  api_key = request.cookies.get('api_key')
  level = int(request.args.get('level'))
  if api_key:
    users = User.query.filter_by(api_key = api_key).all()
    if users:
      user = users[0]
      user.level = level
      db.session.add(user)
      db.session.commit()
      return "200 Ok"
    else:
      return "No such User"
  else:
      return "No such User"
@app.route('/api/v1/updateachievement',methods=['GET','POST'])
def updateachievement():
  api_key = request.cookies.get('api_key')
  new_achievement = request.args.get('achieved')
  print new_achievement
  users = User.query.filter_by(api_key = api_key)
  user = users[0]
  facebook_id = user.fb_id
  url = 'https://graph.facebook.com/' + facebook_id + '/achievements'
  payload = urllib.urlencode({'access_token':ACCESS_TOKEN,
                            'achievement': ACHIEVEMENT_DOMAIN + 'achieves/'+str(new_achievement)})
  req = urllib2.Request(url,payload)
  response = urllib2.urlopen(req)
  return response.read()


@app.route('/',methods=['GET','POST'])
def server_start():
  return render_template('index.html',app_id = APP_ID)
@app.route('/gameplay',methods=['GET','POST'])
def game_play():
  return render_template('gameplay.html')

@app.route('/achieves/<lid>')
def achievements(lid):
  achs = {'1':'Cancer','2':'Metabolism','3':'Immunilogy','4':'Mental Health ','5':'Kineases','6':'Proteases','7':'Transcriptio factors','8':'All rounder '}
  title = achs[lid] + ' Level'
  url = ACHIEVEMENT_DOMAIN + 'achieves/' + lid
  desc = 'Conqured Level %s , %s Category ' %(lid,achs[lid])
  img = ACHIEVEMENT_DOMAIN + 'img/ach' + lid + '.jpeg'
  point = str(int(lid) * 10)
  app_id = APP_ID
  print lid
  return render_template('/ach.html',title = title,url = url,desc = desc,img = img,point = point,app_id = app_id)

# Create DB
def create_db():
  db.create_all()
db.create_all()

if __name__ == '__main__':
    app.debug = True
    app.run()
