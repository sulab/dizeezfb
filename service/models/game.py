from . import db
import datetime

class Game(db.Model):
  id = db.Column(db.Integer,primary_key = True)
  player_id = db.Column(db.String(30))
  player_name = db.Column(db.String(50))
  start_timestamp = db.Column(db.DateTime)
  category = db.Column(db.Integer)
  questions = db.relationship("GQAssociation")
  num_questions = db.Column(db.Integer)
  user_agent = db.Column(db.String(150))
  player_ip = db.Column(db.String(30))

  logs = db.relationship('Log', backref = 'parentGame')

  def __init__(self,player_id = -1, player_name = "Anonymous", category=-1, num_question=-1,ua="",pip=""):
    self.player_id = player_id
    self.player_name = player_name
    self.category = category
    self.num_questions = num_question
    self.start_timestamp = datetime.now()
    self.user_agent = ua
    self.player_ip = pip

  def __repr__(self):
    return "<game %d , Plaer %s>"%(self.id, self.player_name)
