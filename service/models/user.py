from . import db
import datetime

class User(db.Model):
  id          = db.Column(db.Integer,primary_key = True)
  fb_id       = db.Column(db.String(100))
  name        = db.Column(db.String(100))
  api_key     = db.Column(db.Text)
  level       = db.Column(db.Integer,default = 1)

  def __init__(self,name = "",api_key = "",fb_id = ""):
    self.name = name
    self.api_key = api_key
    self.fb_id = fb_id

  def __repr__(self):
    return "<%s>"%(self.name)

  def json_view(self):
    return { 'id'     :self.fb_id,
             'name'   :self.name,
             'api_key':self.api_key,
              'level':self.level }
