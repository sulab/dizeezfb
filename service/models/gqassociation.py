from . import db
import datetime

class GQAssociation(db.Model):
  #-- Game Question Association
  id = db.Column(db.Integer,primary_key = True)
  game_id = db.Column(db.Integer,db.ForeignKey('game.id'))
  question_id = db.Column(db.Integer,db.ForeignKey('question.id'))
  question = db.relationship("Question")
