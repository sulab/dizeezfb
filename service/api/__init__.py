#
#  A P I
#
quest_parser = reqparse.RequestParser()
quest_parser.add_argument('api_key',type=str,location='cookies')

class Questions(Resource):
  '''
    GET: Returns list of questions

  '''
  def get(self):
    args = quest_parser.parse_args()
    user = db.session.query(User).filter_by(api_key=args['api_key']).first()
    if not user:
      return {'error':'not authenticated'},200
    else:
      env =  request.environ
      #category_count = len(Category.query.all())     #-- number of categories
      #category = random.randint(1,category_count)    #-- selects a category at random
      #question_count = len(Category.query.get(category).questions)  #-- number of questions belonging to that category

      #category = random.choice(Category.query.all())   #-- choose a category at random
      #question_count = len(category.questions)

      #Currently method 1 is used. Later a selection process can be implemented
      questions,method = get_questions(args['api_key'],2)         #Previously it was method 1
      question_count = len(questions)
      #if question_count > 10:          # 10 is given for testing purpose
      #  question_count = 10            # if its < 10 will not alter it.

      #category_instance = Category.query.get(category)  #-- selects category instance
      #questions = [ qcassoc.question for qcassoc in category.questions ]      #-- set of question instances
      #random.shuffle(questions)              #-- shuffles the questions inplace

      #game = Game(user.id,user.name,category.id,question_count,env.get('HTTP_USER_AGENT'),env.get('REMOTE_ADDR'))

      game = Game(user.id,user.name,method,question_count,env.get('HTTP_USER_AGENT'),env.get('REMOTE_ADDR'))


      #game.questions = repr([ q.id for q in questions ])
      db.session.add(game)
      db.session.commit()

      # Now Populate GQAssociation table
      for quest in questions[0:question_count] :
        gqassoc = GQAssociation()
        gqassoc.game_id = game.id
        gqassoc.question_id = quest.id
        db.session.add(gqassoc)

      db.session.commit()

      #populate session with needed informations
      session['game_id'] = game.id


      return [i.json_view() for i in questions[0:question_count] ]

class Choices(Resource):
  '''
    POST: writes the log component
  '''
  def post(self):
    data =  json.loads(request.data)
    log  = Log(data['choice_id'],data['text'],data['parentQuestion'],data['correct'],session['game_id'])
    db.session.add(log)
    db.session.commit()


catquest_parser = reqparse.RequestParser()
catquest_parser.add_argument('category',type=str,location='json')
catquest_parser.add_argument('questions',type=str,location='json')

class NewCategoryQuestion(Resource):
  def put(self,**kwargs):
    args = catquest_parser.parse_args()
    catid = Category.query.filter_by(text=args['category']).all()[0].id
    for quest in eval(args['questions']):
      questions =  Question.query.filter_by(text=quest).all()
      for q in questions:
        qcat = QCATAssociation()
        qcat.question_id = q.id
        qcat.category_id = catid
        db.session.add(qcat)
      db.session.commit()



question_parser = reqparse.RequestParser()
question_parser.add_argument('categories',type=str,location='json')
question_parser.add_argument('choices',type=str,location='json')
question_parser.add_argument('quest_text',type=str,location='json')
question_parser.add_argument('correct_choice',type=int,location='json')


#-- Adds a new question. Backend of /addquest

class NewQuestion(Resource):
  def put(self,**kwargs):
    args = question_parser.parse_args()
    choice_ids = []
    category_ids = []
    #-- gets the choice ids (non existent choice will be created)
    for choice_text in eval(args['choices']):
      print choice_text
      choice_ids.append(create_if_not_choice(choice_text))
    print choice_ids
    #-- gets the cateogory ids (non existent category will be created)
    for category_text in eval(args['categories']):
      print category_text
      category_ids.append(create_if_not_category(category_text))
    print category_ids
    q = Question(args['quest_text'])
    q.correct_choice_id = choice_ids[args['correct_choice']]

    db.session.add(q)
    db.session.commit()

    for choice_id in choice_ids:
      qc = QCAssociation()
      qc.question_id = q.id
      qc.choice_id = choice_id
      db.session.add(qc)
    db.session.commit()

    for category_id in category_ids:
      qcat = QCATAssociation()
      qcat.category_id = category_id
      qcat.question_id = q.id
      db.session.add(qcat)
    db.session.commit()

    return "OK",200




user_parser = reqparse.RequestParser()
user_parser.add_argument('name',type=str,location='json')
user_parser.add_argument('api_key',type=str, location='cookies')
user_parser.add_argument('id',type=str,location='json')
class Users(Resource):
  def get(self,**kwargs):
    args = user_parser.parse_args()
    user = db.session.query(User).filter_by(api_key = args['api_key']).first()
    if not user:
      return {'error':'no_user'},200
    else:
      return user.json_view(),200

  def put(self, **kwargs):
    args = user_parser.parse_args()
    user = db.session.query(User).filter_by(fb_id = args['id']).first()
    if not user:
      new_user = User(args['name'],
                       base64.b64encode(hashlib.sha256( str(random.getrandbits(256)) ).digest(), random.choice(['rA','aZ','gQ','hH','hG','aR','DD'])).rstrip('=='),fb_id = args['id'])
      print args['name'],args['id']      #-- debug
      db.session.add(new_user)
      db.session.commit()
      return new_user.json_view(),200
    else:
      return user.json_view(),200

#-- Config API urls
api.add_resource(Questions, '/api/v1/questions')
api.add_resource(Choices,'/api/v1/choices')
api.add_resource(Users,'/api/v1/user')
api.add_resource(NewQuestion,'/newquestion')
api.add_resource(NewCategoryQuestion,'/newcategoryquestion')



