from . import db
import datetime

class Category(db.Model):
  id            = db.Column(db.Integer, primary_key = True)
  text          = db.Column(db.String(240))
  created       = db.Column(db.DateTime)
        #point to QCATAssociaton object, while each such object points to a uniqe question object
        #Many to Many relation on Category , Questions

  questions     = db.relationship("QCATAssociation")

  def __init__(self,ctext):
    self.text  = ctext
    self.created = datetime.now()

  def __repr__(self):
    return "<Category : %s>"%(self.text)
