from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from .category import Category
from .choice import Choice
from .game import Game
from .gqassocation import GQAssociation
from .log import Log
from .qcassociation import QCAssociation
from .qcatassociation import QCATAssociation
from .question import Question
from .user import User
